from django.shortcuts import render, get_object_or_404, redirect, reverse
from .models import Plak
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import PlakCarForm

def main(request):
    # queryset = Plak.objects.all()
    form = PlakCarForm(request.POST or None)
    # query = None
    query = None
    if request.method == "POST":
        if form.is_valid():
            word = form.cleaned_data['word']
            num = form.cleaned_data['num']

            query = Plak.objects.filter(word=word, number=num).first()
            # query = Plak.objects.get(word=word, number=num)
            # query.save()

    context = {
                'queryset': query,
                'form': form
            }

    return render(request, 'ndex.html', context)

# def main(request):
#     # queryset = Plak.objects.all()
#     form = PlakCarForm(request.POST or None)
#     context = {'queryset': None,
#                'form': form}
#     if request.method=="POST":
#         if form.is_valid:
#             # word = form.cleaned_data['word']
#             # num = form.cleaned_data['num']
#             query = Plak.objects.filter(word=form.word, number=form.num)
#             # query.save()
#             context = {
#                 'queryset': query,
#                 'form': form
#             }
#
#     return render(request, 'ndex.html', context)


# def index(request):
    # queryset = Post.objects.filter(featured=True)
    # most_recent = queryset.order_by('-created')
    # paginator = Paginator(most_recent, 1)
    # page_req = 'page'
    # page = request.GET.get(page_req)
    # try:
    #     paginated_queryset = paginator.page(page)
    # except PageNotAnInteger:
    #     paginated_queryset = paginator.page(1)
    # except EmptyPage:
    #     return render(request, 'page_404.html', {})
        # paginated_queryset = paginator.page(paginator.num_pages)

    # context = {
        # 'most_recent': most_recent,
        # 'queryset': paginated_queryset,
        # 'page_req': page_req,
    # }
    # return render(request, 'index.html', context)