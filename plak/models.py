from django.db import models


from django.contrib.auth import get_user_model

User = get_user_model()


class Plak(models.Model):
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    number = models.PositiveIntegerField()
    word = models.CharField(max_length=1, blank=True, null=True)

    def __str__(self):
        return "Post object (12_365{})".format(self.number)
